﻿using System;
using System.Collections.Generic;
using System.Linq;
public static class Kata
{
    public static int Lcm(List<int> nums)
    {
        if (nums.Count == 0)
        {
            return 1;
        }
        if (nums.Contains(0))
        {
            return 0;
        }
        bool CheckMultiply(int dividend, List<int> nums)
        {
            foreach (int number in nums)
            {
                if (dividend % number != 0)
                {
                    return false;
                }
            }
            return true;
        }
        int max = nums.Max();

        int GetMinMultiply(List<int> nums)
        {
            int result = max;
            while (!CheckMultiply(result, nums))
            {
                result += max;
            }
            return result;
        }
        return GetMinMultiply(nums);
    }
}